/**!
 * clickable.js - Делает заданные блоки кликабельными. Ищет внутри блока целевую ссылку link и переходет по ней.
 * Возможно задавать исключения.
 *
 * @param {string} link - ссылка внутри блока, по которой нужно перейти
 * @param {string} excludelink - ссылка-исключение
 */

(function($) {
  $.fn.clickable = function(options) {
    
    var settings = $.extend( {
      'link'         : '.clickable-link',
      'excludelink' : '.clickable-exclude'
    }, options);
    
    var styles = {
      cursor: 'pointer'
    };
    return this.each(function(){
      var $obj = $(this);
      
      $obj.css(styles);
      
      $obj.on('click', function(e){
        if ($(e.target).closest(settings.excludelink).length) { //если клик оказался на ссылке-исключении
          return true;
        }
        else {
          var $link = $obj.find(settings.link),
              loc;
          if (!$link.length)
            $link = $obj.find('a').not(settings.excludelink).eq(0);
          
          loc = $link.attr('href');
          
          if (location !== undefined)
            location.href = loc;
          
        }
      });
    });
    
  };
})(jQuery);
/* clickable.js ends */