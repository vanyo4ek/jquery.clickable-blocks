var gulp           = require('gulp'),
    concat         = require('gulp-concat'),
    sourcemaps     = require('gulp-sourcemaps'),
    processhtml    = require('gulp-processhtml'),
    del            = require('del'),
    filter         = require('gulp-filter'),
    mainBowerFiles = require('main-bower-files'),
    order          = require('gulp-order'),
    cache          = require('gulp-cached'),
    jshint         = require('gulp-jshint'),
    uglify         = require('gulp-uglifyjs');

gulp.task('default', ['watch']);

gulp.task('production', ['uglify']);

gulp.task('watch', function () {
  gulp.watch('src/js/**/*.js', ['lint', 'concat_scripts']);
  gulp.watch('src/html/**/*.html', ['html']);
});

/*Склеиваем внешние js-библиотеки*/
gulp.task('scripts:vendor', function () {
  var vendors  = mainBowerFiles(),
      jsFilter = filter(['**/*.js', '!**/jquery.js'], {restore: true});
  
  return gulp.src(vendors)
    .pipe(jsFilter)
    .pipe(order(vendors))
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('./src/js/'));
});

gulp.task('lint', function () {
  return gulp.src(['./src/js/*.js', '!src/js/vendor.js'])
    .pipe(cache('linting'))
    .pipe(jshint())
    .pipe(jshint.reporter());
});

/*Склеиваем js*/
gulp.task('concat_scripts', function () {
  var scripts = [
    'src/js/clickable.js'
  ];
  
  return gulp.src(scripts)
    .pipe(concat({path: 'jquery.clickable.js'}))
    .pipe(gulp.dest('./dist/'));
});

/*Склеиваем html*/
gulp.task('html', function () {
  var opts = {};
  
  return gulp.src(['./src/html/**/*.html', '!src/html/tpl/*.html'])
    .pipe(processhtml(opts))
    .pipe(gulp.dest('./examples/'));
});

//proruction
gulp.task('uglify', function() {
  gulp.src('dist/jquery.clickable.js')
    .pipe(uglify("jquery.clickable.min.js", {
      mangle: true
    }))
    .pipe(gulp.dest('dist/'))
});